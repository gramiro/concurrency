package estacionamientogui;

import java.util.logging.Level;
import java.util.logging.Logger;

public class Camion extends Vehiculo {

    public Camion() {
        tamaño = 10f;
    }

    @Override
    public void run() {

        while (true) {

            if (estado) {
                try {
                    Estacionamiento.mutexCamion.acquire();
                    Estacionamiento.filaCamion.add(this);
                    Estacionamiento.mutexCamion.release();

                    if (Estacionamiento.filaCamion.size() == 1) { //¿Soy el primero?
                        System.out.println("CAMION: Soy el primero de mi fila");
                        
                    if(Estacionamiento.dormirAparcador.availablePermits() == 0)    
                        Estacionamiento.dormirAparcador.release(); //Lo despierto solo si soy el primero;

                    }

                    System.out.println("CAMION: Llegué a la cola para estacionar, y me duermo");
                    dormir.acquire(); //me duermo;

                    System.out.println("CAMION: Me despierto");

            Thread.sleep((long) (Math.random() * 3000)); //Me voy, y busco mi vehiculo dentro de 1-10 segundos;
                   // Thread.sleep(2000); //Me voy, y busco mi vehiculo dentro de 2 segundos;

                    System.out.println();

                    Estacionamiento.mutexPedido.acquire();
                    System.out.println("CAMION: Llegué a la cola de pedidos");
                    Estacionamiento.filaPedidos.add(this);
                                        Estacionamiento.instance().crearPedido(this);

                    estado = false; //Ya me estacionaron, y cuando vuelvo, vuelvo a retirar mi vehiculo;
                    Estacionamiento.mutexPedido.release();

                } catch (InterruptedException ex) {
                    Logger.getLogger(Camion.class.getName()).log(Level.SEVERE, null, ex);
                }
            } else {

                try {
                    if (Estacionamiento.filaPedidos.size() == 1) { //¿Soy el primero?
                        System.out.println("CAMION: Soy el primero en la cola de pedidos y despierto al APARCADOR");
                        Estacionamiento.dormirAparcador.release(); //Lo despierto solo si soy el primero;
                    }
                    System.out.println("CAMION: Estoy en la fila de pedidos y me duermo");
                    dormir.acquire(); //me duermo;
                    System.out.println("CAMION: Estaba en la fila de pedidos, me despertaron y me dieron el vehiculo. Nos vemos!");
                    break;

                } catch (InterruptedException ex) {
                    Logger.getLogger(Camion.class.getName()).log(Level.SEVERE, null, ex);
                }
            }
        }
    }
}
