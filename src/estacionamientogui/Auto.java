package estacionamientogui;

import java.util.logging.Level;
import java.util.logging.Logger;

public class Auto extends Vehiculo {

    public Auto() {
        tamaño = 2.5f;
    }

    @Override
    public void run() {

        while (true) {

            if (estado) {
                try {
                    Estacionamiento.mutexAuto.acquire();
                    Estacionamiento.filaAuto.add(this);
                    Estacionamiento.mutexAuto.release();

                    if (Estacionamiento.filaAuto.size() == 1) { //¿Soy el primero?
                        System.out.println("AUTO: Soy el primero de mi fila");
                        //Especificar si es el primero o segundo.
                        //Agregar estado del aparcador para chequear cuando esta durmiendo y cuando no.
                        
                        if(Estacionamiento.dormirAparcador.availablePermits() == 0)    
                            Estacionamiento.dormirAparcador.release(); //Lo despierto solo si soy el primero;

                    }

                    System.out.println("AUTO: Llegué a la cola para estacionar, y me duermo");
                    dormir.acquire(); //me duermo;

                    System.out.println("AUTO: Me despierto");

            Thread.sleep((long) (Math.random() * 3000)); //Me voy, y busco mi vehiculo dentro de 1-10 segundos;
           //         Thread.sleep(2000); //Me voy, y busco mi vehiculo dentro de 2 segundos;

                    System.out.println();
                    
                    Estacionamiento.mutexPedido.acquire();
                    System.out.println("AUTO: Llegué a la cola de pedidos");
                    Estacionamiento.filaPedidos.add(this);
                    Estacionamiento.instance().crearPedido(this);
                    estado = false; //Ya me estacionaron, y cuando vuelvo, vuelvo a retirar mi vehiculo;
                    Estacionamiento.mutexPedido.release();

                } catch (InterruptedException ex) {
                    Logger.getLogger(Auto.class.getName()).log(Level.SEVERE, null, ex);
                }
            } else {
                try {
                    if (Estacionamiento.filaPedidos.size() == 1) { //¿Soy el primero?
                        System.out.println("AUTO: Soy el primero en la cola de pedidos y despierto al APARCADOR");
                        Estacionamiento.dormirAparcador.release(); //Lo despierto solo si soy el primero;
                    }
                    System.out.println("AUTO: Estoy en la fila de pedidos y me duermo");
                    dormir.acquire(); //me duermo;
                    System.out.println("AUTO: Estaba en la fila de pedidos, me despertaron y me dieron el vehiculo");
                    break;

                } catch (InterruptedException ex) {
                    Logger.getLogger(Auto.class.getName()).log(Level.SEVERE, null, ex);
                }
            }
        }
    }
}
