package estacionamientogui;

import java.util.ArrayList;
import java.util.LinkedList;
import java.util.logging.Level;
import java.util.logging.Logger;

public class Aparcador implements Runnable {

    ArrayList ar = new ArrayList();
    ArrayList plazas = new ArrayList();

    public Aparcador() {
        for (int i = 0; i < 50; i++) {
            plazas.add(new Plaza());
        }
    }

    @Override
    public void run() {
        while (true) {
            try {
                Estacionamiento.instance().modificarEstadoDeAparcador("Trabajando");
                if (!(Estacionamiento.filaAuto.size() != 0
                        || Estacionamiento.filaFurgoneta.size() != 0
                        || Estacionamiento.filaCamion.size() != 0
                        || Estacionamiento.filaPedidos.size() != 0)) {

                    System.out.println("APARCADOR: Me duermo");
                    Estacionamiento.instance().modificarEstadoDeAparcador("Durmiendo");
                    Estacionamiento.dormirAparcador.acquire();
                    Estacionamiento.instance().modificarEstadoDeAparcador("Trabajando");

                    System.out.println("APARCADOR: Alguien me despertó");

                }

                Fila filaElegida = elegirRandom();

                if (ar.isEmpty()) {
                    System.out.println("APARCADOR: No elegi ninguna fila");
                    Estacionamiento.instance().modificarEstadoDeAparcador("Durmiendo");
                    Estacionamiento.dormirAparcador.acquire(); //Si no elegi nadie, me duermo;
                } else {
                    // Estacionamiento.espacioLibre -= filaElegida.getFirst().tamaño; //Mal, porque si elige pedido, no hay que restar;

                    Vehiculo vehiculoElegido = filaElegida.getFirst();

                    //Aqui le damos el ticket al vehiculo.
                    vehiculoElegido.dormir.release();

                    // sSystem.out.println("APARCADOR: Hay " + Estacionamiento.espacioLibre + " lugares libres");
                    if (filaElegida.tipo == 1) {

                        System.out.println("APARCADOR: Elegi un AUTO");

                        Estacionamiento.mutexAuto.acquire();
                        ((Plaza) plazas.get(vehiculoElegido.numeroDePlaza)).espacioLibre -= filaElegida.getFirst().tamaño;
                        System.out.println("APARCADOR: Estaciono el AUTO");
                        Estacionamiento.filaAuto.removeFirst();
                        Thread.sleep(1000);
                        Estacionamiento.instance().eliminarAuto();
                        Estacionamiento.espacioLibre += 2.5;
                        Estacionamiento.instance().modificarBarra();
                                               Estacionamiento.mutexAuto.release();

                    } else if (filaElegida.tipo == 2) {
                        System.out.println("APARCADOR: Elegi una FURGONETA");

                        Estacionamiento.mutexFurgoneta.acquire();
                        ((Plaza) plazas.get(vehiculoElegido.numeroDePlaza)).espacioLibre -= filaElegida.getFirst().tamaño;
                        System.out.println("APARCADOR: Estaciono la FURGONETA");
                        Estacionamiento.filaFurgoneta.removeFirst();
                        Thread.sleep(1000);
                        Estacionamiento.instance().eliminarFurgoneta();

                                                Estacionamiento.espacioLibre += 5;
                        Estacionamiento.instance().modificarBarra();

                        Estacionamiento.mutexFurgoneta.release();
                    } else if (filaElegida.tipo == 3) {

                        System.out.println("APARCADOR: Elegi un CAMION");

                        Estacionamiento.mutexCamion.acquire();
                        ((Plaza) plazas.get(vehiculoElegido.numeroDePlaza)).espacioLibre -= filaElegida.getFirst().tamaño;
                        System.out.println("APARCADOR: Estaciono el CAMION");
                        Estacionamiento.filaCamion.removeFirst();
                        Thread.sleep(500);
                        Estacionamiento.instance().eliminarCamion();
                                                Estacionamiento.espacioLibre += 10;
                                                                        Estacionamiento.instance().modificarBarra();

                                                 

                        Estacionamiento.mutexCamion.release();
                    } else {                                            //ES UN PEDIDO;
                        System.out.println("APARCADOR: Elegi un PEDIDO");
                        Estacionamiento.mutexPedido.acquire();
                        Vehiculo vehiculo = filaElegida.getFirst();
                        ((Plaza) plazas.get(vehiculoElegido.numeroDePlaza)).espacioLibre += filaElegida.getFirst().tamaño;
                        System.out.println("APARCADOR: Atiendo el PEDIDO de la plaza " + vehiculoElegido.numeroDePlaza);
                        Estacionamiento.filaPedidos.removeFirst();
                        Thread.sleep(100);
                        Estacionamiento.instance().eliminarPedido();
                        Estacionamiento.espacioLibre -= vehiculo.tamaño;
                        Estacionamiento.instance().modificarBarra();

                        Estacionamiento.mutexPedido.release();
                    }

                }
            } catch (InterruptedException ex) {
                Logger.getLogger(Aparcador.class.getName()).log(Level.SEVERE, null, ex);
            }
        }
    }

    public Fila elegirRandom() {

        ar.clear();

        int c = -1;
        int a = -1;
        int f = -1;
        int i;

        if (!(Estacionamiento.filaAuto.size() == 0
                && Estacionamiento.filaCamion.size() == 0
                && Estacionamiento.filaFurgoneta.size() == 0)) {
            for (i = 0; i < 50; i++) {
                if (((Plaza) plazas.get(i)).espacioLibre >= 10
                        && Estacionamiento.filaCamion.size() > 0
                        && !ar.contains(Estacionamiento.filaCamion)) {
                    ar.add(Estacionamiento.filaCamion);

                    c = i;

                }

                if (((Plaza) plazas.get(i)).espacioLibre >= 5
                        && Estacionamiento.filaFurgoneta.size() > 0
                        && !ar.contains(Estacionamiento.filaFurgoneta)) {
                    ar.add(Estacionamiento.filaFurgoneta);

                    f = i;

                }

                if (((Plaza) plazas.get(i)).espacioLibre >= 2.5
                        && Estacionamiento.filaAuto.size() > 0
                        && !ar.contains(Estacionamiento.filaAuto)) {
                    ar.add(Estacionamiento.filaAuto);

                    a = i;

                }

                if (c != -1 && a != -1 && f != -1) {
                    break;
                }

                if (c != -1 && f != -1 && Estacionamiento.filaAuto.size() == 0) {
                    break;
                }
                if (c != -1 && a != -1 && Estacionamiento.filaFurgoneta.size() == 0) {
                    break;
                }
                if (c != -1 && Estacionamiento.filaAuto.size() == 0 && Estacionamiento.filaFurgoneta.size() == 0) {
                    break;
                }

                if (f != -1 && c != -1 && Estacionamiento.filaAuto.size() == 0) {
                    break;
                }
                if (f != -1 && a != -1 && Estacionamiento.filaCamion.size() == 0) {
                    break;
                }
                if (f != -1 && Estacionamiento.filaAuto.size() == 0 && Estacionamiento.filaCamion.size() == 0) {
                    break;
                }

                if (a != -1 && f != -1 && Estacionamiento.filaFurgoneta.size() == 0) {
                    break;
                }
                if (a != -1 && c != -1 && Estacionamiento.filaFurgoneta.size() == 0) {
                    break;
                }
                if (a != -1 && Estacionamiento.filaCamion.size() == 0 && Estacionamiento.filaFurgoneta.size() == 0) {
                    break;
                }

                if ((Estacionamiento.filaAuto.size() > 0 && a != -1)
                        || (Estacionamiento.filaCamion.size() > 0 && c != -1)
                        || (Estacionamiento.filaFurgoneta.size() > 0 && f != -1)) {
                    continue;
                }
            }
            //     System.out.println("Indice: " + i);

        }

        if (Estacionamiento.filaPedidos.size() > 0) {
            ar.add(Estacionamiento.filaPedidos);
        }

        if (ar.size() == 0) {
            return null;
        }

        double seleccion = Math.random() * 10;
        //   System.out.println("APARCADOR:  El numero random es: " + seleccion);

        int j = (int) (seleccion % ar.size());

       // System.out.println("APARCADOR: El MOD eeeees: " + j + a + c + f);
      //  System.out.println("APARCADOR: EL TAMAÑO DEL AR ES: " + ar.size());
        Vehiculo vehiculoElegido = ((Fila) ar.get(j)).getFirst();
        if (ar.get(j).equals(Estacionamiento.filaAuto)) {
            vehiculoElegido.numeroDePlaza = a;
        } else if (ar.get(j).equals(Estacionamiento.filaFurgoneta)) {
            vehiculoElegido.numeroDePlaza = f;
        } else if (ar.get(j).equals(Estacionamiento.filaCamion)) {
            vehiculoElegido.numeroDePlaza = c;
        }

        System.out.println("APARCADOR: La plaza elegida es: " + vehiculoElegido.numeroDePlaza);

        return (Fila) ar.get(j);
    }

}
